package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.ASSISTANT;
import static org.junit.Assert.*;

public class EmployeeMockTest {
    private EmployeeMock employeeMock = new EmployeeMock();
    DidacticFunction didacticFunction = ASSISTANT;

    @Before
    public void setUp() throws Exception {
        EmployeeMock employeeMock = new EmployeeMock();
    }

    @After
    public void tearDown() throws Exception {
        employeeMock = null;
    }

    @Test
    public void TC1() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "2940704310737", didacticFunction, 400.0)), true);
    }

    @Test
    public void TC2() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "abc", didacticFunction, 400.0)), false);
    }

    @Test
    public void TC3() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "2940704", didacticFunction, 400.0)), false);
    }

    @Test
    public void TC4() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "29407043107377", didacticFunction, 1.00)), false);
    }

    @Test
    public void TC5() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "294070431073", didacticFunction, 1.00)), false);
    }

    @Test
    public void TC6() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "2940704310737", didacticFunction, 2.00)), true);
    }

    @Test
    public void TC7() {
        assertEquals(employeeMock.addEmployee(new Employee("Clark", "Kent", "2940704310737", didacticFunction, 0.0)), false);
    }

    /**
     * LAB - 4
     * TC01_WBT
     */
    @Test
    public void modifyEmployeeFunction1() {
        Employee e1=null;
        String employeeMockList = employeeMock.getEmployeeList().toString();
        employeeMock.modifyEmployeeFunction(e1,DidacticFunction.ASSISTANT);
        assertTrue(employeeMock.getEmployeeList().toString().equals(employeeMockList));
    }

    /**
     * TC02_WBT
     */
    @Test
    public void modifyEmployeeFunction2() {
        Employee e1 = new Employee();
        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(ASSISTANT);
        e1.setSalary(1000.0);
        employeeMock.addEmployee(e1);
        //List<Employee> employeeMockList = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(e1.getFunction() == DidacticFunction.LECTURER);
    }

    /**
     * TC03_WBT
     */
    @Test
    public void modifyEmployeeFunction3() {
        Employee e1 = new Employee();
        e1.setId(100);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(ASSISTANT);
        e1.setSalary(1000.0);
        employeeMock.addEmployee(e1);
        List<Employee> employeeMockList = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(e1, DidacticFunction.TEACHER);
        assertTrue(employeeMock.getEmployeeList().equals(employeeMockList));
    }
}